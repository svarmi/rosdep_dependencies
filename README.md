# Rosdep dependencies

Svarmi's custom rosdep dependency list.

Rosdep documentation: http://docs.ros.org/independent/api/rosdep/html/

# Wait, what is this exactly?

When you run rosdep on a ROS package:

```
rosdep check <package>
```

Rosdep will read the package's package.xml and look for dependencies required by this package.

When rosdep finds unresolved dependencies it will search for a way to install them by looking at the files in `/etc/ros/rosdep/sources.list.d/`. By default there is a file there named `20-default.list` which links to various yaml files containing information about how rosdep should install dependencies.

ROS's list of dependency install methods is not complete so occasionally rosdep will not find how to install a dependency by looking at `20-default.list`. To solve that problem we put the file `10-svarmi.list` to `/etc/ros/rosdep/sources.list.d` and from the `10-svarmi.list` file we point to `dependencies.yaml` which contains information on how to install dependencies that are missing in ROS's default `20-default.list`. For further info on how this works refer to the rosdep documentation.

# Setting up a machine to use Svarmi's extended rosdep list

Basically you just need to put `10-svarmi.list` into `/etc/ros/rosdep/sources.list.d` and run `rosdep update`. We have a script for this:

```
cd ~
git clone git@gitlab.com:svarmi/scripts
./scripts/rosdep/inject_custom_rosdep_settings.py -h
./scripts/rosdep/inject_custom_rosdep_settings.py HOSTNAME
```

# Adding new entries to dependencies.yaml

If you add a new dependency to `dependencies.yaml` you need to run `rosdep update` on all machines that are using our extended dependency list, this is because rosdep uses a local cache to remember the dependency list.

```
cd ~
git clone git@gitlab.com:svarmi/scripts
./scripts/rosdep/update_rosdep_on_all_machines.py -h
```
